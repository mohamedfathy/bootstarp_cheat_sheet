# Clearfix

## Clearfix
```
<div class="clearfix">...</div>
```

## Clearfix
```
<div class="container">
  <div class="bg-info  clearfix">
    <button type="button" class="btn btn-secondary float-left">Example Button floated left</button>
    <button type="button" class="btn btn-secondary float-right">Example Button floated right</button>
  </div>   
  <p>item</p>
</div>
```