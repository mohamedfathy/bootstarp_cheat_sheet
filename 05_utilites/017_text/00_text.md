# Text

## Text alignment
```
<p class="text-justify">...</p>
<p class="text-right">...</p>
<p class="text-left">...</p>
<p class="text-center">...</p>
```

## Text wrapping and overflow
```
<div class="badge badge-primary text-wrap" style="width: 6rem;">
  This text should wrap.
</div>

<div class="text-nowrap" style="width: 8rem;">
  This text should overflow the parent.
</div>
```

## text truncate
```
<!-- Block level -->
<div class="row">
  <div class="col-2 text-truncate">
    Praeterea iter est quasdam res quas ex communi.
  </div>
</div>

<!-- Inline level -->
<span class="d-inline-block text-truncate" style="max-width: 150px;">
  Praeterea iter est quasdam res quas ex communi.
</span>
```

## Word break
```
<p class="text-break"></ps>
```

## Text Transform
```
<p class="text-lowercase">Lowercased text.</p>
<p class="text-uppercase">Uppercased text.</p>
<p class="text-capitalize">CapiTaliZed text.</p>
```

## font weight and italic
```
<p class="font-weight-bold">Bold text.</p>
<p class="font-weight-bolder">Bolder weight text (relative to the parent element).</p>
<p class="font-weight-normal">Normal weight text.</p>
<p class="font-weight-light">Light weight text.</p>
<p class="font-weight-lighter">Lighter weight text (relative to the parent element).</p>
<p class="font-italic">Italic text.</p>
```

## Monospace
```
<p class="text-monospace">This is in monospace</p>
```

## Reset Color
```
<p class="text-muted">
  Muted text with a <a href="#" class="text-reset">reset link</a>.
</p>
```

## Text decoration
```
<a href="#" class="text-decoration-none">Non-underlined link</a>
```
