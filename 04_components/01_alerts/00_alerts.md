# Alerts

## Alerts
```
<div class="alert alert-primary" role="alert">
  A simple primary alert—check it out!
</div>
alert-primary
alert-secondary
alert-success
alert-danger
alert-warning
alert-info
alert-light
alert-dark
```

## Dismiss
```
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
```

## Methods
```
$().alert('close')	
```

## Events
```
$('#myAlert').on('closed.bs.alert', function () {
  // do something...
})
```