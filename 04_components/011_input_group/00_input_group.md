# Input Group

## Input Group
```
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">$</span>
  </div>
  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
  <div class="input-group-append">
    <span class="input-group-text">.00</span>
  </div>
</div>
```

## Sizing
```
<div class="input-group input-group-sm">
<div class="input-group input-group-lg">
```