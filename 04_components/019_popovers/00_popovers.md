# Popovers

## Popovers
```
<button type="button" class="btn btn-lg btn-danger" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?">Click to toggle popover</button>

<script>
  $(function () {
    $('[data-toggle="popover"]').popover()
  })
</script>
```

## Dismiss on next click
```
<a tabindex="0" class="btn btn-lg btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?">Dismissible popover</a>

$('.popover-dismiss').popover({
  trigger: 'focus'
})
```
