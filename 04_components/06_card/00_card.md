# Card

## Card
```
<div class="card">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>
```

## Card Element
```
<div class="card">
...
</div>
```

## Card Body
```
<div class="card-body">
...
</div>
```

## Card Title
```
<h3 class="card-title">
...
</h3>
```

## Card text
```
<h3 class="card-text">
...
</h3>
```

## Card link
```
<h3 class="card-link">
...
</h3>
```

## Card header
```
<div class="card-header">
  ...
</div>
```

## card footer
```
<div class="card-footer">
  ...
</div>
```

## card Images
```
<img src="..." class="card-img-top" alt="...">
```

## Image overlays
```
<div class="card bg-dark text-white">
  <img src="..." class="card-img" alt="...">
  <div class="card-img-overlay">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    <p class="card-text">Last updated 3 mins ago</p>
  </div>
</div>
```


## Card groups
```
<div class="card-group">
  <div class="card">
  ...
  </div>
  <div class="card">
  ...
  </div>
  <div class="card">
  ...
  </div>
</div>
```


