# Images

## Responsive Images
```
<img src="..." class="img-fluid" alt="Responsive image">
```

## Image thumbnails
```
<img src="..." alt="..." class="img-thumbnail">
```

## Aligning images
```
<img src="..." class="rounded float-left" alt="...">
<img src="..." class="rounded mx-auto d-block" alt="...">

```
