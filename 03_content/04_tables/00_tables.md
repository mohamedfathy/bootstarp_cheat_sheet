# Tables

## Tables
```
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
```

### table dark
```
<table class="table table-dark">
```

### Table head options
```
 <thead class="thead-dark">
 <thead class="thead-light">
```

### Striped rows
```
<table class="table table-striped">
```

### Bordered table
```
<table class="table table-bordered">
```

### Hoverable rows
```
<table class="table table-hover">
```

### Captions
```
<table class="table">
  <caption>List of users</caption>
  <thead>
```

### Responsive tables
```
<div class="table-responsive">
  <table class="table">
    ...
  </table>
</div>
```