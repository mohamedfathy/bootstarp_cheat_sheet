# Code

## Inline Code
```
<p>For example, <code>&lt;section&gt;</code> should be wrapped as inline.</p>
```


## Code Blocks
- Use <pre>s for multiple lines of code.
```
<pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>
```

## User input
```
To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br>
To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd>
```

## Sample output
```
<samp>This text is meant to be treated as sample output from a computer program.</samp>
```