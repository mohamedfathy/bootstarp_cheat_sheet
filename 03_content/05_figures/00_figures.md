# Figures

## Figures
```
<figure class="figure">
  <img src="..." class="figure-img img-fluid rounded" alt="...">
  <figcaption class="figure-caption">A caption for the above image.</figcaption>
</figure>
```

### table dark
```
<table class="table table-dark">
```

### Table head options
```
 <thead class="thead-dark">
 <thead class="thead-light">
```

### Striped rows
```
<table class="table table-striped">
```

### Bordered table
```
<table class="table table-bordered">
```

### Hoverable rows
```
<table class="table table-hover">
```

### Captions
```
<table class="table">
  <caption>List of users</caption>
  <thead>
```

### Responsive tables
```
<div class="table-responsive">
  <table class="table">
    ...
  </table>
</div>
```